let minutes = 0, seconds = 0, milliseconds = 0, resultTime = 0;
let minHtml = document.querySelector('.minutes');
let secHtml = document.querySelector('.seconds');
let millHtml = document.querySelector('.milliseconds');

let startBtn = document.querySelector('#start');
let pauseBtn = document.querySelector('#pause');
let stopBtn = document.querySelector('#stop');

startBtn.addEventListener('click', startTimer);
stopBtn.addEventListener('click', stop);
pauseBtn.addEventListener('click', pause);


function startTimer() {
  resultTime = setInterval( () => {
    milliseconds++;
    if (milliseconds === 100) {
      milliseconds = 0;
      seconds++;
    };
    if (seconds === 60) {
      seconds = 0;
      minutes++;
    }
    showTimer();
  }, 10)
}

function stop() {
  clearInterval(resultTime);
  minutes = 0, seconds = 0, milliseconds = 0;
  millHtml.innerText = '00';
  secHtml.innerText = '00';
  minHtml.innerText = '00';
}

function pause() {
  clearInterval(resultTime);
}

function showTimer() {
  minHtml.textContent = addZero(minutes);
  secHtml.textContent = addZero(seconds);
  millHtml.textContent = milliseconds;
}

function addZero(time) {
  if (time < 10) {
    return `0${time}`;
  }
  return time + '';
}