let objectLaptop = {
  name: "Lenovo",
  model: "ThinkPad",
  price: "800$",
  color: "black",
  size: {
    width: 30,
    height: 25
  }
};

console.log( objectLaptop );

let newObject = {};

function cloneObj(obj) {
  for (let key in obj) {
    if (obj[key] == "object") {
      newObject[key] = cloneObj(obj[key]);
    } else {
      newObject[key] = obj[key];
    }
  }
  return newObject;
}

let cloneLaptop = cloneObj( objectLaptop );

console.log( cloneLaptop );

newObject.name = "Asos";

console.log( newObject );