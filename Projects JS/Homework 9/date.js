function getUserAge() {
  let birthday = prompt('Enter your birth date');
  let reversBirthDate = birthday
    .split('.')
    .reverse()
    .join('-');
  let birthDate = new Date(reversBirthDate);
  let month = birthDate.getMonth();
  let date = birthDate.getDate();
  let currentDate = new Date();

  let age = currentDate.getFullYear() - birthDate.getFullYear();
  if ( currentDate.getMonth() <= month && currentDate.getDate() < date ) 
    age--;
  alert(`You are ${age} years old`);

  // Zodiac start
  
  if (date >= 21 && month === 3 || date <= 20 && month === 4) alert('Ваш знак зодиака: Овен');

  else if (date >= 21 && month === 4 || date <= 21 && month === 5) alert('Ваш знак зодиака: Телец');

  else if (date >= 21 && month === 5 || date <= 21 && month === 6) alert('Ваш знак зодиака: Близнецы');
   
  else if (date >= 22 && month === 6 || date <= 22 && month === 7) alert('Ваш знак зодиака: Рак');
  
  else if (date >= 23 && month === 7 || date <= 23 && month === 8) alert('Ваш знак зодиака: Лев');

  else if (date >= 24 && month === 8 || date <= 23 && month === 9) alert('Ваш знак зодиака: Дева');

  else if (date >= 24 && month === 9 || date <= 23 && month === 10) alert('Ваш знак зодиака: Весы');
  
  else if (date >= 24 && month === 10 || date <= 22 && month === 11) alert('Ваш знак зодиака: Скорпион');
  
  else if (date >= 23 && month === 11 || date <= 21 && month === 12) alert('Ваш знак зодиака: Стрелец');
  
  else if (date >= 22 && month === 12 || date <= 20 && month === 1) alert('Ваш знак зодиака: Козерог');
  
  else if (date >= 21 && month === 1 || date <= 20 && month === 2) alert('Ваш знак зодиака: Водолей');
  
  else if (date >= 21 && month === 2 || date <= 20 && month === 3) alert('Ваш знак зодиака: Рыбы');

};

console.log(getUserAge());