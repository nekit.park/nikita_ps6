const movies = [
  { title: 'a', year: 2017, rating: 4.7 },
  { title: 'b', year: 2018, rating: 4.5 },
  { title: 'c', year: 2018, rating: 3 },
  { title: 'd', year: 2015, rating: 4.2 }
];

const excludeMovies = [
  { title: 'a', year: 2017, rating: 4.7 },
  { title: 'b', year: 2018, rating: 4.5 },
  { title: 'r', year: 2018, rating: 3 },
  { title: 'l', year: 2015, rating: 4.2 }
];

function excludeBy(arr1, arr2, title){
  return arr1.filter(obj => {
    for (let obj2 of arr2) 
      if (obj[title] === obj2[title]) return false;
    return true;
  });
}

const output = excludeBy(movies, excludeMovies, 'title');
console.log(output);