function CreateNewUser(){
  this.firstName = prompt('Enter your first name');
  this.lastName = prompt('Enter your last name');
  this.getLogin = () => {
    let firstLetter = this.firstName
      .split('', 1)
      .join()
      .toLowerCase();
    let userLogin = firstLetter + '.' + this.lastName.toLowerCase();
    return userLogin;
  }
};

const newUser = new CreateNewUser();
console.log(newUser);
console.log(newUser.getLogin());
