let btn = document.querySelector('.button');
    
btn.addEventListener('click', () => {
    let inputDiameter = document.createElement('input');
    let btnDraw = document.createElement('button');

    inputDiameter.id = 'diameter';
    inputDiameter.placeholder = 'Enter diameter in px';
    document.body.appendChild(inputDiameter);

    btnDraw.id = 'draw';
    btnDraw.innerText = 'Draw a circle';
    document.body.appendChild(btnDraw);

    let btn = document.querySelector('.button');
    btn.remove();

    btnDraw.addEventListener('click', createCircles);

});

function createCircles() {
    let diameter = document.getElementById('diameter');
    document.getElementById('draw').remove();
    document.getElementById('diameter').remove();

    for (let i = 0; i < 100; i++) {

        const br = document.createElement('br');
        if(i % 10 === 0 ) {
        document.body.appendChild(br);
        }

        let circle = document.createElement('div');
        circle.classList.add('circle');
        circle.style.width = circle.style.height = diameter.value + 'px';
        circle.style.borderRadius = '50%';
        circle.style.display = 'inline-block';
        circle.style.marginRight = '10px';
        circle.style.backgroundColor = randomColor();
        document.body.appendChild(circle);
    }
    document.querySelectorAll('div').forEach(function (circle) {
        circle.onclick = () => circle.style.visibility = 'hidden';
    })
}
function randomColor() {
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += 'ABCDEF01234567890'.charAt(Math.round(Math.random() * 15))
    }
    return color;
}
