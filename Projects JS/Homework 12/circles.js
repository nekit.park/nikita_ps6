let btn = document.querySelector('.button');

btn.addEventListener('click', (event) => {
  event.preventDefault();
  btn.remove();
  let inputDiameter = document.createElement('input');
  let inputColor = document.createElement('input');
  let drawBtn = document.createElement('button');

  inputDiameter.classList.add('diameter');
  inputColor.classList.add('color');

  let body = document.querySelector('body');

  body.appendChild(inputDiameter);
  body.appendChild(inputColor);
  body.appendChild(drawBtn);

  inputDiameter.style.marginRight = '15px';
  inputDiameter.placeholder = 'Enter Diameter';
  inputColor.placeholder = 'Enter Color';
  drawBtn.textContent = 'Lets draw some circle!'
  drawBtn.style.marginLeft = '15px';

  drawBtn.addEventListener('click', (event) => {
    
    let div = document.createElement('div');
    
    let diameter = inputDiameter.value;
    let color = inputColor.value;

    div.style.width = diameter + 'px';
    div.style.height = diameter + 'px';
    div.style.backgroundColor = color;
    div.style.borderRadius = '50%';
    
    body.appendChild(div);
  })

})