let userName = prompt("Your Name?");
let userAge = prompt("Your age?");

while (userAge === null || userAge === "" || isNaN(userAge)) {
    userAge = prompt("Please use numbers, Your age?");
}

while (userName === null || userName === "" || !isNaN(userName)) {
    userName = prompt("Please use letters, Your Name?");
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
}   else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
}   else {
    let userAnswer = confirm("Are you sure you want to continue?");
    if (userAnswer) {
        alert(`Welcome, ${userName}`);
    }   else {
        alert("You are not allowed to visit this website");
    }
}